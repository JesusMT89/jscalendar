/**
 * Created by jumz on 02/11/2016.
 */

var initialsWeekES = ["Lun", "Mar", "Mie", "Jue", "Vie", "Sab", "Dom"];
var monthsES = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
var labelsES = ["Fecha de inicio: ","Fecha de fin: ","Formato de fecha: "];

var initialsWeekEN = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
var monthsEN = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
var labelsEN = ["Start date: ","End date: ","Format date: "];

var id;

//Al iniciar
window.onload = function () {
    date = new Date();
    document.getElementById("idStartDate").value = null;
    document.getElementById("idEndDate").value = null;
};

//diferencia el boton del que viene la llamada(START/END)
function startButton() {
    startCalendar("idSetStartDate");
}

function endButton() {
    startCalendar("idSetEndDate");
}

function resetStart() {
    reset("idResetStartDate");
}

function resetEnd() {
    reset("idResetEndDate");
}

//Hacemos click en button calendario...
function startCalendar(idButton) {
//cogemos el id del boton que pulsamos
    id = idButton;

//habilitamos el calendario y deshabilitamos los botones de borrado
    document.getElementById("calendar").style.visibility = "visible";
    document.getElementById("idResetStartDate").style.visibility = "hidden";
    document.getElementById("idResetEndDate").style.visibility = "hidden";

//fecha actual
    dayweek = date.getDay();
    today = date.getDate();
    month = date.getMonth();
    year = date.getFullYear();

// Elementos del header de calendario
    tit = document.getElementById("titles");
    previous = document.getElementById("previous");
    next = document.getElementById("next");
    date1 = document.getElementById("idStartDate");
    date2 = document.getElementById("idEndDate");

    header();
    paintTh();
    paintDays();
}

//cargamos el header
function header() {
    if (spanish.className == 'activado') {
        tit.innerHTML = monthsES[month] + " " + year;
    }else{
        tit.innerHTML = monthsEN[month] + " " + year;
    }
    var goPrevious = month - 1;
    var goNext = month + 1;

    //si se pasa...
    if (goPrevious < 0) {
        goPrevious = 11;
    }
    if (goNext > 11) {
        goNext = 0;
    }
}

//pintamos el th de los dias de la semana.
function paintTh() {
    var english = document.getElementById("english");
    for (i = 0; i < 7; i++) {
        if (spanish.className == 'activado'){
            document.getElementById("row0").getElementsByTagName("th")[i].innerHTML = initialsWeekES[i];
        }else{
            document.getElementById("row0").getElementsByTagName("th")[i].innerHTML = initialsWeekEN[i];
        }
    }
}

//rellenar td's con los días
function paintDays() {
    //Buscar numeroDia de la semana del dia 1:
    firstDay = new Date(year, month, "1");
    var placeFirstDay = firstDay.getDay();
    placeFirstDay--; //restamos uno para que el lunes sea el primer día
    if (placeFirstDay == -1) {
        placeFirstDay = 6;
    }

    //buscar fecha para primera celda:
    var diaprmes = firstDay.getDate();
    var prcelda = diaprmes - placeFirstDay;
    var startDay = firstDay.setDate(prcelda);
    var dayInFirstPlace = new Date();
    dayInFirstPlace.setTime(startDay); //dayInFirstPlace=fecha primera celda.

    //Recorremos la tabla y rellenamos
    for (i = 1; i < 7; i++) {
        fila = document.getElementById("row" + i);
        for (j = 0; j < 7; j++) {
            var thisDay = dayInFirstPlace.getDate();
            thisMonth = dayInFirstPlace.getMonth();
            thisYear = dayInFirstPlace.getFullYear();

            var td = fila.getElementsByTagName("td")[j];
            td.innerHTML = thisDay;

            //estilo td's
            td.style.backgroundColor = "white";
            td.style.color = "black";
            td.style.fontSize = "16px";
            td.style.opacity = 1;
            td.value = "valid";

            if (j == 6) {
                td.style.background = "red";
            }

            if (thisMonth != month) {
                td.value = "null";
                td.style.fontSize = "12px";
                td.style.opacity = 0.5;
            }

            if (thisMonth == date.getMonth() && thisDay == date.getDate() && thisYear == date.getFullYear()) {
                td.style.backgroundColor = "darkcyan";
            }

            //pasar al siguiente día
            thisDay = thisDay + 1;
            dayInFirstPlace.setDate(thisDay);
        }
    }
}

//seleccionamos el dia y lo pintamos
function selectDate(event) {
    var day;

    //si el dia que se pincha es del mes que se muestra...
    if (event.target.value != "null") {
        day = event.target.innerHTML;
        var month = thisMonth;
        var year = thisYear;
        var format = document.getElementById("idSelectFormat").value;

        //si es diciembre le restamos uno y ponemos el mes 12
        if (thisMonth == 0) {
            month = 12;
            year = year - 1;
        }

        //segun el id del boton que se pulse...
        if ((id == "idSetStartDate") || (id == "idResetStartDate")) {
            document.getElementById("idResetEndDate").style.visibility = "hidden";
            if (format == "dd/mm/aaaa") {
                date1.value = day + "/" + month + "/" + year;
                document.getElementById("idResetStartDate").style.visibility = "visible";
            } else {
                date1.value = month + "/" + day + "/" + year;
                document.getElementById("idResetStartDate").style.visibility = "visible";
            }
        } else {
            document.getElementById("idResetStartDate").style.visibility = "hidden";
            if (format == "dd/mm/aaaa") {
                date2.value = day + "/" + month + "/" + year;
                document.getElementById("idResetEndDate").style.visibility = "visible";
            } else {
                date2.value = month + "/" + day + "/" + year;
                document.getElementById("idResetEndDate").style.visibility = "visible";
            }
        }
    }
    dateDifference();
}

//Ver mes anterior
function previousMonth() {
    var date = new Date();
    firstDay--;
    date.setTime(firstDay);
    month = date.getMonth();
    year = date.getFullYear();

    header();
    paintDays();
}

//ver mes posterior
function nextMonth() {
    var date = new Date();
    var miliseconds = firstDay.getTime();
    miliseconds = miliseconds + (45 * 24 * 60 * 60 * 1000);
    date.setTime(miliseconds);
    month = date.getMonth();
    year = date.getFullYear();

    header();
    paintDays();
}

//borrar fechas introducidas
function reset(idButton) {
    //cogemos el id del boton que pulsamos
    id = idButton;

    if (id == "idResetStartDate") {
        document.getElementById("idResetStartDate").style.visibility = "hidden";
        date1.value = null;
    } else {
        document.getElementById("idResetEndDate").style.visibility = "hidden";
        date2.value = null;
    }
    document.getElementById("idBetweenDates").innerHTML = "";
}

//esconder el calendario
function hideCalendar() {
    document.getElementById("calendar").style.visibility = "hidden";
    document.getElementById("idResetStartDate").style.visibility = "hidden";
    document.getElementById("idResetEndDate").style.visibility = "hidden";

}

//restar fechas y mostrarlas por pantalla
function dateDifference() {
    var format = document.getElementById("idSelectFormat").value;
    var startDateValue = document.getElementById("idStartDate").value;
    var endDateValue = document.getElementById("idEndDate").value;
    var subtract = 0;
    var startDate = startDateValue.split('/');
    var endDate = endDateValue.split('/');
    var finalStartDate;
    var finalEndDate;

    //restas para los diferentes formatos
    if (format == "dd/mm/aaaa") {
        finalStartDate = Date.UTC(startDate[2], startDate[1], startDate[0]); //yyyy-mm-dd
        finalEndDate = Date.UTC(endDate[2], endDate[1], endDate[0]);
    } else {
        finalStartDate = Date.UTC(startDate[2], startDate[0], startDate[1]);
        finalEndDate = Date.UTC(endDate[2], endDate[0], endDate[1]);
    }

    if ((startDateValue != "") && (endDateValue != "")) {
        if (finalEndDate > finalStartDate) {
            subtract = finalEndDate - finalStartDate;
        } else {
            subtract = finalStartDate - finalEndDate;
        }
        //mostramos los dias ylos pintamos
        var finalSubtract = subtract / (1000 * 60 * 60 * 24);
        var year = 0;
        var rest = 0;
        if (finalSubtract > 365) {
            year = Math.floor(finalSubtract / 365);
            rest = finalSubtract % 365;
            document.getElementById("idBetweenDates").innerHTML = "Hay " + year + " años y " + rest + " días de diferencia";
        } else {
            document.getElementById("idBetweenDates").innerHTML = "Hay " + finalSubtract + " días de diferencia";
        }
    } else {
        document.getElementById("idBetweenDates").innerHTML = "";
    }
}

//Al cambiar el formato ponemos a cero los campos
function changeFormat() {
    date1.value = null;
    date2.value = null;
    document.getElementById("idBetweenDates").innerHTML = "";
    document.getElementById("idResetStartDate").style.visibility = "hidden";
    document.getElementById("idResetEndDate").style.visibility = "hidden";
    document.getElementById("calendar").style.visibility = "hidden";
}

//Cambiamos de idioma
function changeLanguage(event) {
    var id = event.target;
    var nav = document.getElementById('navbar').getElementsByTagName('button');
    var labels = document.getElementsByTagName('label');

    //clase default para todos los botones
    for (i=0;i<nav.length;i++){
        nav[i].className = "default";
    }

    //clase activado para el boton pulsado
    id.className="activado";

    if(id.id == 'spanish') {
        for (i = 0; i < labels.length; i++) {
            labels[i].innerHTML = labelsES[i];
        }
    }else{
        for (i = 0; i < labels.length; i++) {
            labels[i].innerHTML = labelsEN[i];
        }
    }

    header();
    paintTh();

}
